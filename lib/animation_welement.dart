import 'package:fimber/fimber.dart';
import 'package:flame/components.dart';
//import 'package:flame/gestures.dart';
import 'package:flame/src/gestures/events.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';

import 'extensions/asset_extension.dart';
import 'extensions/sprite_animation_extension.dart';
import 'extensions/vector_extension.dart';
import 'group_welement.dart';
import 'state_machine_data.dart';
import 'welement.dart';
import 'welement_source.dart';

class AnimationWelement extends Welement with Tappable {
  static const animationFileName = 'animation.json';
  static const atlasFileName = 'atlas.webp';
  static const idleStateName = 'idle';

  SpriteAnimationComponent? spriteAnimationComponent;

  AnimationWelement({
    required String canvasName,
    required Vector2 canvasSize,
    required double globalScale,
    required WelementSource welementSource,
    required Vector2 screenSize,
    GroupWelement? group,
  }) : super(
          canvasName: canvasName,
          canvasSize: canvasSize,
          globalScale: globalScale,
          welementSource: welementSource,
          screenSize: screenSize,
          group: group,
        );

  @override
  void onMount() async {
    super.onMount();

    position
        .setFrom(welementSource.position.toVector2DependsOfSize(canvasSize));
    anchor = welementSource.anchor;
    _initAnimation();
  }

  void _initAnimation() async {
    //clearChildren();

    spriteAnimationComponent = await loadSpriteAnimationComponent();
    size.setFrom(spriteAnimationComponent!.size);
    addChild(spriteAnimationComponent!);

    Fimber.i('animation position $position size $size');
  }

  Future<SpriteAnimationComponent> loadSpriteAnimationComponent() async {
    final spriteAnimation = await SpriteAnimationExtension.fromFolderWithAtlas(
        pathAtlas, pathAnimation);
    final animationSize =
        spriteAnimation.currentFrame.sprite.srcSize * ownScale;
    return SpriteAnimationComponent(
      animation: spriteAnimation,
      size: animationSize,
      removeOnFinish: false,
    );
  }

  @mustCallSuper
  @override
  void onStateUpdated() {
    super.onStateUpdated();
    _initAnimation();
  }

  @mustCallSuper
  @override
  void update(double t) {
    super.update(t);
    final isLastFrame =
        spriteAnimationComponent?.animation?.isLastFrame ?? false;
    if (isLastFrame) {
      sendActionFsm(OnEndAction());
    }
  }

  @override
  bool onTapDown(TapDownInfo event) {
    // see notes for this property
    lastTapDownDetails = event;

    _pushEffects(event);

    return true;
  }

  void _pushEffects(TapDownInfo event) {
    // resolve without detect an opacity:
    // tap by many frames is confusing
    resolver.push(OnTapAction(event), this);
  }

  /// Look at atlas for welement.
  String get pathAtlas => '$canvasName/$name/$atlasFileName';

  Future<bool> get isPathAtlasExists async => pathAtlas.isAssetExists;

  /// Look at data for welement depends of current state.
  String get pathAnimation => buildPathData(canvasName, name, stateName);

  Future<bool> get isPathAnimationExists async => pathAnimation.isAssetExists;

  static Future<bool> isAnimation(String canvasName, String name) async =>
      buildPathData(canvasName, name, idleStateName).isAssetExists;

  static String buildPathData(
    String canvasName,
    String name,
    String stateName,
  ) =>
      'images/$canvasName/$name/$stateName/$animationFileName';

  @override
  Map<String, dynamic> toJson() => super.toJson()
    ..addAll({
      'path atlas': pathAtlas,
      //'is path atlas exists': isPathAtlasExists,
      'path data': pathAnimation,
      //'is path data exists': isPathDataExists,
    });
}

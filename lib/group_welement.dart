import 'package:fimber/fimber.dart';
import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flutter/widgets.dart';

import 'extensions/string_extension.dart';
import 'extensions/vector_extension.dart';
import 'levelers/group-position-iteration-leveler.dart';
import 'sprite_welement.dart';
import 'welement.dart';
import 'welement_source.dart';

class GroupWelement extends Welement {
  final Vector2 canvasSize;
  final double globalScale;

  int get count => children.length;

  bool get hasChildren => count > 0;

  SpriteWelement? get first =>
      //(count > 0) ? children[0] as SpriteWelement : null;
      (count > 0) ? children.first as SpriteWelement : null;

  SpriteWelement? get second =>
      //(count > 1) ? children[1] as SpriteWelement : null;
      (count > 1) ? children.elementAt(1) as SpriteWelement : null;

  SpriteWelement? get last =>
      (count > 0) ? children.last as SpriteWelement : null;

  GroupWelement({
    required String canvasName,
    required WelementSource welementSource,
    required Vector2 screenSize,
    required this.canvasSize,
    required this.globalScale,
  }) : super(
          canvasName: canvasName,
          canvasSize: canvasSize,
          globalScale: globalScale,
          welementSource: welementSource,
          screenSize: screenSize,
        );

  @override
  void onMount() async {
    super.onMount();

    position.setFrom(Vector2.zero());
    anchor = welementSource.anchor;
    if (welementSource.anchor != WelementSource.defaultAnchor) {
      Fimber.w(
          'An anchor does not apply to the grouped elements and will be ignored.'
          ' GroupWelement: `$this`');
    }

    if (welementSource.scale != 1.0) {
      Fimber.w('An scale the different from 1.0 does not apply to the grouped'
          ' elements and will be ignored.'
          ' GroupWelement: `$this`');
    }

    if (welementSource.group.isEmpty) {
      Fimber.w('The group for welement is empty. GroupWelement: $this');
      return;
    }

    //welementSource.group.forEach((ws) async { - Don't do inner async forEach!
    for (final WelementSource ws in welementSource.group) {
      if (ws.isVisible) {
        final ew = SpriteWelement(
          canvasName: canvasName,
          welementSource: ws,
          screenSize: screenSize,
          canvasSize: canvasSize,
          globalScale: globalScale,
          group: this,
        );
        ew.anchor = ws.anchor;
        await addChild(ew);
      }
    }

    sendActionLeveler(GroupPositionIterationActionLeveler());
  }

  @override
  Map<String, dynamic> toJson() {
    final r = super.toJson()
      ..addAll({
        'canvas size': canvasSize.toJson1(),
        'global scale': globalScale.n1,
      });
    if (children.isNotEmpty) {
      r.addAll({
        'children count': count,
      });
      final jsonChildrenList = [];
      propagateToChildren((dynamic child) {
        final jsonChildren = <String, dynamic>{
          'position': [child.position.x, child.position.y],
          'size': [child.size.x, child.size.y],
        };
        jsonChildrenList.add(jsonChildren);
        return true;
      });
      r.addAll({'children': jsonChildrenList});
    }
    return r;
  }

  /// Calculate a rectangle size of group.
  Vector2 calculateGroupSize() {
    if (children.isEmpty) {
      return Vector2.zero();
    }

    Rect? bounds;
    propagateToChildren((dynamic child) {
      final rect = child.toRect();
      bounds = (bounds != null) ? bounds!.expandToInclude(rect) : rect;
      return true;
    });

    return bounds!.size.toVector2();
  }
}

import 'package:fimber/fimber.dart';
import 'package:flutter/foundation.dart';

import 'leveler.dart';
import '../extensions/anchor_extension.dart';
import '../extensions/vector_extension.dart';
import '../group_welement.dart';
import '../welement.dart';

class GroupPositionIterationActionLeveler extends ActionLeveler {
  @mustCallSuper
  @override
  bool process(Welement we, double dt) {
    super.process(we, dt);
    final group = we as GroupWelement;
    Fimber.i('Process `${group.name}`... $tick');

    // \todo Work with any size of group.
    if (group.count != 2) {
      Fimber.w('We work with only 2 welements.'
          ' We have ${group.count}.'
          ' Other welements will be ignored.');
    }

    // calculate a distance between 2 welements
    final a = group.first!;
    final b = group.second!;

    // before relocation to screen device
    final pictureSize = we.canvasSize;
    final directionBefore =
        a.welementSource.position.toVector2DependsOfSize(pictureSize) -
            b.welementSource.position.toVector2DependsOfSize(pictureSize);

    // after relocation to screen device
    final directionAfter = a.position - b.position;

    // save the distance
    // \todo This is a roughly save: the position at the corners can be lost.
    final delta = directionBefore - directionAfter;
    if ((a.anchor.byLeft && b.anchor.byCenter) ||
        (a.anchor.byRight && b.anchor.byCenter)) {
      b.position -= delta;
    } else if ((b.anchor.byLeft && a.anchor.byCenter) ||
        (b.anchor.byRight && a.anchor.byCenter)) {
      a.position -= delta;
    }

    /* - positioning depends of center screen
    final centerScreen = we.unscaledScreenSize.toRect().center.toVector2();
    final ac = (centerScreen - a.position).length2;
    final bc = (centerScreen - b.position).length2;
    if (ac > bc) {
      a.position -= delta;
    } else {
      b.position -= delta;
    }
    */

    return true;
  }
}

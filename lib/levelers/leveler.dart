import 'package:flutter/foundation.dart';

import '../welement.dart';

class Leveler {
  final Welement welement;

  final List<ActionLeveler> actionLevelerList;

  bool get isEmpty => actionLevelerList.isEmpty;

  bool get isNotEmpty => actionLevelerList.isNotEmpty;

  Leveler({
    required this.welement,
    List<ActionLeveler>? actionLevelerList,
  }) : actionLevelerList = actionLevelerList ?? <ActionLeveler>[];

  void process(double dt) {
    final completed = <ActionLeveler>[];
    actionLevelerList.forEach((a) {
      if (a.process(welement, dt)) {
        completed.add(a);
      }
    });

    completed.forEach((c) => actionLevelerList.remove(c));
  }
}

abstract class ActionLeveler {
  int tick = 0;

  bool get isFirstTick => tick == 0;

  /// \return true when action is completed.
  @mustCallSuper
  bool process(Welement we, double dt) {
    ++tick;
    return false;
  }
}

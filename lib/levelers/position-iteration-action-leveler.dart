import 'package:fimber/fimber.dart';
import 'package:flame/extensions.dart';
import 'package:flutter/foundation.dart';

import 'leveler.dart';
import '../welement.dart';

class PositionIterationActionLeveler extends ActionLeveler {
  double maxSquare = -double.maxFinite;
  late double lastMaxSquare;
  late Vector2 lastPosition;

  Vector2 accDistance = Vector2.zero();

  @mustCallSuper
  @override
  bool process(Welement we, double dt) {
    super.process(we, dt);
    Fimber.i('Process `${we.name}`... $tick');

    if (isWholeInsideTheScreen(we)) {
      return true;
    }

    //final delta = Vector2(we.position.x - we.unscaledScreenRect);

    final distance = Vector2(-10, 0);
    accDistance += distance;
    final newPosition = we.position + accDistance;
    we.position.setFrom(newPosition);

    final intersectRect = calculateIntersectSquareWithScreen(we);
    final isOverlap = intersectRect.width > 0 && intersectRect.height > 0;
    if (!isOverlap) {
      // continue, rectangles are not overlapped
      return false;
    }

    // will continue while a square grows
    final square = intersectRect.width * intersectRect.height;
    if (square > maxSquare) {
      lastMaxSquare = maxSquare;
      lastPosition = we.position;
      maxSquare = square;
      return false;
    }

    // we found a nice position
    //Fimber.i('We found a nice position for ${we.name}');
    if (lastMaxSquare > square) {
      we.position.setFrom(lastPosition);
    }

    return true;
  }

  Rect calculateIntersectSquareWithScreen(Welement we) {
    final weRect = we.toRect();
    return we.unscaledScreenRect.intersect(weRect);
  }

  bool isWholeInsideTheScreen(Welement we) {
    final weRect = we.toRect();
    return we.unscaledScreenRect.contains(weRect.topLeft) &&
        we.unscaledScreenRect.contains(weRect.topRight) &&
        we.unscaledScreenRect.contains(weRect.bottomRight) &&
        we.unscaledScreenRect.contains(weRect.bottomLeft);
  }
}

import 'package:fimber/fimber.dart';
import 'package:flutter/foundation.dart';

import 'leveler.dart';
import '../welement.dart';

class RemoveInvisibleActionLeveler extends ActionLeveler {
  static const int percentLimitForHide = 40;

  @mustCallSuper
  @override
  bool process(Welement we, double dt) {
    super.process(we, dt);
    Fimber.i('Process `${we.name}`... $tick');

    if (we.isBackground) {
      // doesn't hide background
      return true;
    }

    final weViewedPercent = we.viewedPercent();
    final needHide = (we.isPartBackground && weViewedPercent == 0) ||
        (!we.isPartBackground && weViewedPercent < percentLimitForHide);
    Fimber.i('we `${we.name}`'
        ' viewed percent $weViewedPercent'
        ' hide? $needHide');

    if (needHide) {
      we.isVisible = false;
    }

    if (!we.isVisible) {
      we.remove();
    }

    return true;
  }
}

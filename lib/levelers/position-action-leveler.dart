import 'package:fimber/fimber.dart';
import 'package:flame/extensions.dart';
import 'package:flutter/foundation.dart';

import 'leveler.dart';
import '../extensions/vector_extension.dart';
import '../utils.dart';
import '../welement.dart';

class PositionActionLeveler extends ActionLeveler {
  @mustCallSuper
  @override
  bool process(Welement we, double dt) {
    super.process(we, dt);
    Fimber.i('Process `${we.name}` tick $tick');

    final needPosition = Utils.scaleToRangeVector2(
      we.position,
      Vector2.zero(),
      we.canvasSize,
      Vector2.zero(),
      we.unscaledScreenSize,
    );
    Fimber.i('align position ${we.position.s1} => ${needPosition.s1}');
    we.position.setFrom(needPosition);

    return true;
  }
}

import 'package:fimber/fimber.dart';
import 'package:flame/extensions.dart';
import 'package:flutter/foundation.dart';

import 'leveler.dart';
import '../utils.dart';
import '../welement.dart';

class ScaleActionLeveler extends ActionLeveler {
  @mustCallSuper
  @override
  bool process(Welement we, double dt) {
    super.process(we, dt);
    Fimber.i('Process `${we.name}`... $tick');

    if (we.isBackground || we.isPartBackground) {
      // doesn't scale background and it parts
      return true;
    }

    Vector2 neededSize =
        Utils.correctTooBigSize(we.size, we.unscaledScreenSize);
    neededSize = Utils.correctTooSmallSize(neededSize, we.unscaledScreenSize);
    we.localScale = neededSize.x / we.size.x;
    we.size.setFrom(neededSize);

    return true;
  }
}

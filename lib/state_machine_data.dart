import 'package:fimber/fimber.dart';
//import 'package:flame/gestures.dart';
import 'package:flame/src/gestures/events.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';

import 'effects/weffect.dart';
import 'extensions/json_extension.dart';
import 'extensions/weffect_extension.dart';
import 'welement.dart';

// Run in the terminal for generate this file:
//   flutter packages pub run build_runner build
// \see https://medium.com/flutter-community/generate-the-code-to-parse-your-json-in-flutter-c68aa89a81d9
//part 'state_machine_data.g.dart';

class StateMachineData {
  final List<WTransition> transitionList;
  final List<WDirector> directorList;

  const StateMachineData({
    List<WTransition> transitions = const <WTransition>[],
    List<WDirector> directors = const <WDirector>[],
  })  : transitionList = transitions,
        directorList = directors;

  factory StateMachineData.fromString(String s) =>
      StateMachineData.fromJson(s.json);

  factory StateMachineData.fromJson(Map<String, dynamic> json) {
    final tl = (json['transitions'] ?? []) as List;
    final transitions = tl.map((s) => WTransition.fromJson(s)).toList();

    final dl =
        (json['directors'] ?? <String, dynamic>{}) as Map<String, dynamic>;
    final directors = <WDirector>[];
    for (final state in dl.keys) {
      final e = {state: dl[state]};
      directors.add(WDirector.fromJson(e));
    }

    return StateMachineData(transitions: transitions, directors: directors);
  }

  Map<String, dynamic> toJson() {
    final r = <String, dynamic>{};
    if (transitionList.isNotEmpty) {
      r['transitions'] = transitionList.map((e) => e.toJson()).toList();
    }

    if (directorList.isNotEmpty) {
      r['directors'] = <String, dynamic>{};
      for (final director in directorList) {
        final effects = director.effectList
            .map((e) => <String, dynamic>{
                  e.name: e.toJson(),
                })
            .toList();
        r['directors'][director.stateName] = effects;
      }
    }

    return r;
  }

  @override
  String toString() => toJson().sjson;

  @override
  bool operator ==(Object b) =>
      (b is StateMachineData) &&
      listEquals(transitionList, b.transitionList) &&
      listEquals(directorList, b.directorList);

  @override
  int get hashCode => toJson().hashCode;
}

class WTransition {
  static const String separator = ' -> ';

  final String from;
  final String action;
  final String to;

  const WTransition(
    this.from,
    this.action,
    this.to,
  )   : assert(from.length > 0),
        assert(action.length > 0),
        assert(to.length > 0);

  factory WTransition.fromString(String s) => WTransition.fromJson(s);

  factory WTransition.fromJson(String json) {
    final l = json.split(separator);
    if (l.length != 3) {
      Fimber.e('The transitions should be contains 3 parts'
          ' separated by `$separator`. Actual: ${l.length}');
      return WTransition('', '', '');
    }

    final from = l[0].trim();
    final action = l[1].trim();
    final to = l[2].trim();
    return WTransition(from, action, to);
  }

  String toJson() {
    return [from, action, to].join(separator);
  }

  @override
  String toString() => toJson();

  @override
  bool operator ==(Object b) =>
      (b is WTransition) && from == b.from && action == b.action && to == b.to;

  @override
  int get hashCode => [from, action, to].hashCode;
}

class WDirector {
  final String stateName;
  final List<WEffect> effectList;

  WDirector(
    this.stateName,
    this.effectList,
  ) : assert(stateName.length > 0) {
    Fimber.i('Created `$stateName` with effects ${effectList.sjson}');
  }

  void runAll(Welement we) => effectList.forEach((effect) => effect.run(we));

  factory WDirector.fromString(String s) => WDirector.fromJson(s.json);

  factory WDirector.fromJson(Map<String, dynamic> json) {
    final stateName = json.keys.first.trim();
    final el = (json[stateName] ?? []) as List<dynamic>;
    final effects =
        el.map((e) => (e as Map<String, dynamic>).buildEffect).toList();

    return WDirector(stateName, effects);
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      stateName: effectList
          .map((e) => <String, dynamic>{
                e.name: e.toJson(),
              })
          .toList(),
    };
  }

  @override
  String toString() => toJson().sjson;

  @override
  bool operator ==(Object b) =>
      (b is WDirector) &&
      stateName == b.stateName &&
      listEquals(effectList, b.effectList);

  @override
  int get hashCode => toJson().hashCode;
}

class WActions {
  static const String onEnd = 'OnEnd';
  static const String onTap = 'OnTap';
}

abstract class WAction {
  final String name;

  const WAction(this.name);

  @override
  bool operator ==(Object b) => (b is WAction) && name == b.name;

  @override
  int get hashCode => name.hashCode;

  @override
  String toString() => name;
}

/// An animation is action also.
class OnEndAction extends WAction {
  const OnEndAction() : super(WActions.onEnd);
}

class OnTapAction extends WAction {
  final TapDownInfo event;

  const OnTapAction(this.event) : super(WActions.onTap);
}

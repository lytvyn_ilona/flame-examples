import 'package:collection/collection.dart';
import 'package:fimber/fimber.dart';

import 'effects/play_sound_by_tap.dart';
import 'extensions/string_extension.dart';
import 'state_machine_data.dart';
import 'welement.dart';

class Resolver {
  /// Time in seconds for wait before applying the effects.
  /// Experimentally chosen.
  static const double periodBetweenPushes = 0.9;

  final effectDataList = PriorityQueue<EffectData>();

  /// \see runTopPushed()
  void push(WAction action, Welement welement) {
    Fimber.i('Push `$action` for `${welement.name}`'
        ' with order ${welement.order}');
    effectDataList.add(EffectData(action: action, welement: welement));
  }

  static double _accTimeBetweenPushes = 0;

  /// Run the top effects and clear the effect data list.
  /// \see push()
  void runTopPushed(double dt) {
    if (effectDataList.isEmpty) {
      return;
    }

    _accTimeBetweenPushes += dt;
    Fimber.i('effectDataList count ${effectDataList.length}'
        ' ${_accTimeBetweenPushes.s2} / ${periodBetweenPushes.s2}');
    if (_accTimeBetweenPushes < periodBetweenPushes) {
      return;
    }

    _accTimeBetweenPushes = 0;
    run(effectDataList.first);
    effectDataList.removeAll();
  }

  Future<void> run(EffectData effectData) async {
    Fimber.i('Run before send action to FSM'
        ', effectDataList for current state $effectData');
    final w = effectData.welement;
    final isSent = w.sendActionFsm(effectData.action);
    if (!isSent) {
      // attempt play a default sound for this entity
      if (!w.isMute) {
        PlaySoundByTap().run(w);
      }
    }
  }
}

class EffectData implements Comparable<EffectData> {
  final WAction action;
  final Welement welement;

  int get order => welement.order;

  const EffectData({
    required this.action,
    required this.welement,
  });

  @override
  int compareTo(EffectData other) {
    return other.order - order;
  }

  @override
  String toString() => 'action $action for `${welement.name}`'
      ' with state `${welement.stateName}`';
}

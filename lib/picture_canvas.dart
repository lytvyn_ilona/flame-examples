import 'package:flutter/material.dart' hide Animation;
import 'package:flame/game.dart';
import 'package:flame/extensions.dart';

import 'picture_game.dart';
import 'picture_source.dart';

class PictureCanvas extends StatefulWidget {
  final PictureSource pictureSource;

  PictureCanvas({
    required this.pictureSource,
  });

  @override
  PictureCanvasState createState() => PictureCanvasState(pictureSource);
}

class PictureCanvasState extends State<PictureCanvas> {
  final PictureSource pictureSource;

  PictureCanvasState(this.pictureSource);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final pictureGame = PictureGame.fromPictureSource(
      pictureSource: pictureSource,
      screenSize: MediaQuery.of(context).size.toVector2(),
    );

    return _buildGameWidget(pictureGame);
  }

  Widget _buildGameWidget(PictureGame pictureGame) {
    return ClipRect(
      child: GameWidget(game: pictureGame),
    );
  }
}

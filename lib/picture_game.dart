import 'dart:ui';
import 'package:fimber/fimber.dart';
import 'package:flame/components.dart';
import 'package:flame/game.dart';
//import 'package:flame/gestures.dart';
import 'package:flame/src/gestures/events.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_sizer/flutter_sizer.dart';

import 'animation_welement.dart';
import 'background_welement.dart';
import 'group_welement.dart';
import 'picture_source.dart';
import 'extensions/string_extension.dart';
import 'sprite_welement.dart';
import 'utils.dart';
import 'welement.dart';
import 'welement_source.dart';

class PictureGame extends BaseGame with HasTappableComponents {
  static const double periodFps = 24;

  /// For debug.
  static const showWholePicture = false;

  final PictureSource pictureSource;

  final Vector2 screenSize;

  late BackgroundWelement background;

  Vector2 get canvasSize => pictureSource.size;

  double get globalScale => Utils.scaleCoverSize(canvasSize, screenSize);

  double get fitScale => Utils.scaleFitSize(canvasSize, screenSize);

  double get currentScale => showWholePicture ? fitScale : globalScale;

  PictureGame._create({required this.pictureSource, required this.screenSize}) {
    debugMode = kDebugMode;
  }

  static PictureGame fromPictureSource({
    required PictureSource pictureSource,
    required Vector2 screenSize,
  }) {
    return PictureGame._create(
      pictureSource: pictureSource,
      screenSize: screenSize,
    );
  }

  @override
  Future<void> onLoad() async {
    await _initBackground();
    await _initEntitiesAndGroups();
  }

  Future<void> _initBackground() async {
    background = BackgroundWelement(
      canvasName: pictureSource.name,
      welementSource: pictureSource.background,
      screenSize: screenSize,
      canvasSize: canvasSize,
      globalScale: globalScale,
    );
    await add(background);
  }

  Future<void> _initEntitiesAndGroups() async {
    //pictureSource.welementsSource.forEach((ws) async { - Don't do inner async forEach!
    for (final WelementSource ws in pictureSource.welementSourceList) {
      if (!ws.isVisible) {
        continue;
      }

      if (await ws.isAnimation(pictureSource.name)) {
        Fimber.i('`${ws.name}` is animation!');
        final w = AnimationWelement(
          canvasName: pictureSource.name,
          welementSource: ws,
          screenSize: screenSize,
          canvasSize: canvasSize,
          globalScale: globalScale,
        );
        await add(w);
        continue;
      }

      final w = ws.isGroup
          ? GroupWelement(
              canvasName: pictureSource.name,
              welementSource: ws,
              screenSize: screenSize,
              canvasSize: canvasSize,
              globalScale: globalScale,
            )
          : SpriteWelement(
              canvasName: pictureSource.name,
              welementSource: ws,
              screenSize: screenSize,
              canvasSize: canvasSize,
              globalScale: globalScale,
            );
      await add(w);
    }
  }

  @override
  @mustCallSuper
  void update(double dt) {
    super.update(dt);

    components.forEach((we) => updateWelement(we as PositionComponent, dt));

    if (debugMode) {
      logFps(dt);
    }
  }

  void updateWelement(PositionComponent we, double dt) {
    we.propagateToChildren((dynamic child) {
      if (child is Welement) {
        child.processScreenAlignment(dt);
      }
      return true;
    });

    if (we is Welement) {
      we.processScreenAlignment(dt);
    }
  }

  @override
  @mustCallSuper
  void onTapDown(int pointerId, TapDownInfo event) {
    super.onTapDown(pointerId, event);
    Fimber.i('${event.eventPosition} ${event.eventPosition}');
  }

  @override
  @mustCallSuper
  void render(Canvas canvas) {
    canvas.scale(currentScale);
    super.render(canvas);
  }

  static double _accTimeFps = 0;

  void logFps(double dt) {
    _accTimeFps += dt;
    if (_accTimeFps > periodFps) {
      Fimber.i('fps ${fps().n1}');
      _accTimeFps = 0;
    }
  }
}

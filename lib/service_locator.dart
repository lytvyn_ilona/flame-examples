import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'audio.dart';
import 'resolver.dart';
import 'welement_order_counter.dart';

/// Service locator for managers and services.
final GetIt sl = GetIt.instance;

initServiceLocator([BuildContext? context]) {
  // should be first
  sl.registerSingleton(WelementOrderCounter());

  sl.registerSingleton(Audio());
  sl.registerSingleton(Resolver());
}

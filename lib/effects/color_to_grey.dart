import 'package:flutter/animation.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../converters/curve_json_converter.dart';
import '../effects/weffect.dart';
import '../welement.dart';
import 'sprite_component_render_effect.dart';

// Run in the terminal for generate this file:
//   flutter packages pub run build_runner build
// \see https://medium.com/flutter-community/generate-the-code-to-parse-your-json-in-flutter-c68aa89a81d9
part 'color_to_grey.g.dart';

@JsonSerializable(includeIfNull: false)
class ColorToGrey extends WEffect {
  ColorToGrey({
    double duration = 1.0,
    Curve curve = CurveJsonConverter.defaultCurve,
  }) : super(
          name: 'ColorToGrey',
          duration: duration,
          curve: curve,
        );

  @override
  @mustCallSuper
  void run(Welement we) {
    super.run(we);

    final effect = SpriteComponentRenderEffect(
      duration: duration,
      startColorFilterMatrix:
          SpriteComponentRenderEffect.identityColorFilterMatrix,
      endColorFilterMatrix: SpriteComponentRenderEffect.greyColorFilterMatrix,
      onComplete: () => onComplete(we),
    );

    we.addEffect(effect);
  }

  factory ColorToGrey.fromJson(Map<String, dynamic> json) =>
      _$ColorToGreyFromJson(json);

  @mustCallSuper
  @override
  Map<String, dynamic> toJson() =>
      super.toJson()..addAll(_$ColorToGreyToJson(this));
}

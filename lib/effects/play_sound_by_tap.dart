import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../effects/weffect.dart';
import '../audio.dart';
import '../service_locator.dart';
import '../welement.dart';

// Run in the terminal for generate this file:
//   flutter packages pub run build_runner build
// \see https://medium.com/flutter-community/generate-the-code-to-parse-your-json-in-flutter-c68aa89a81d9
part 'play_sound_by_tap.g.dart';

/// Play the sound for welement by tap.
@JsonSerializable(includeIfNull: false)
class PlaySoundByTap extends WEffect {
  Audio get audio => sl.get<Audio>();

  final double volume;

  PlaySoundByTap({
    this.volume = 1.0,
  })  : assert(volume >= 0),
        super(name: 'PlaySoundByTap');

  @override
  @mustCallSuper
  void run(Welement we) {
    super.run(we);

    audio.play(PlayData(
      canvasName: we.canvasName,
      welementName: we.name,
      volume: volume,
    ));
  }

  factory PlaySoundByTap.fromJson(Map<String, dynamic> json) =>
      _$PlaySoundByTapFromJson(json);

  @mustCallSuper
  @override
  Map<String, dynamic> toJson() =>
      super.toJson()..addAll(_$PlaySoundByTapToJson(this));

  @override
  String toString() {
    return '${objectRuntimeType(this, name)}'
        '(volume: $volume'
        ')';
  }
}

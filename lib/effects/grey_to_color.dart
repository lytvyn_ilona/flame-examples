import 'package:flutter/animation.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../converters/curve_json_converter.dart';
import '../effects/weffect.dart';
import '../welement.dart';
import 'sprite_component_render_effect.dart';

// Run in the terminal for generate this file:
//   flutter packages pub run build_runner build
// \see https://medium.com/flutter-community/generate-the-code-to-parse-your-json-in-flutter-c68aa89a81d9
part 'grey_to_color.g.dart';

@JsonSerializable(includeIfNull: false)
class GreyToColor extends WEffect {
  GreyToColor({
    double duration = 1.0,
    Curve curve = CurveJsonConverter.defaultCurve,
  }) : super(
          name: 'GreyToColor',
          duration: duration,
          curve: curve,
        );

  @override
  @mustCallSuper
  void run(Welement we) {
    super.run(we);

    final effect = SpriteComponentRenderEffect(
      duration: duration,
      startColorFilterMatrix: SpriteComponentRenderEffect.greyColorFilterMatrix,
      endColorFilterMatrix:
          SpriteComponentRenderEffect.identityColorFilterMatrix,
      onComplete: () => onComplete(we),
    );

    we.addEffect(effect);
  }

  factory GreyToColor.fromJson(Map<String, dynamic> json) =>
      _$GreyToColorFromJson(json);

  @mustCallSuper
  @override
  Map<String, dynamic> toJson() =>
      super.toJson()..addAll(_$GreyToColorToJson(this));
}

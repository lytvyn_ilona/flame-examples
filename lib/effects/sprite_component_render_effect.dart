import 'package:fimber/fimber.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/painting.dart';

import '../extensions/string_extension.dart';

typedef ColorFilterMatrix = List<double>;

class SpriteComponentRenderEffect extends ComponentEffect<SpriteComponent> {
  static const greyColorFilterMatrix = <double>[
    // greyscale
    0.2126, 0.7152, 0.0722, 0, 0,
    0.2126, 0.7152, 0.0722, 0, 0,
    0.2126, 0.7152, 0.0722, 0, 0,
    0, 0, 0, 1, 0
  ];

  static const identityColorFilterMatrix = <double>[
    // identity
    1, 0, 0, 0, 0,
    0, 1, 0, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 0, 1, 0,
  ];

  static const inverseColorFilterMatrix = <double>[
    // inverse
    -1, 0, 0, 0, 255,
    0, -1, 0, 0, 255,
    0, 0, -1, 0, 255,
    0, 0, 0, 1, 0,
  ];

  static const sepiaColorFilterMatrix = <double>[
    // sepia
    0.393, 0.769, 0.189, 0, 0,
    0.349, 0.686, 0.168, 0, 0,
    0.272, 0.534, 0.131, 0, 0,
    0, 0, 0, 1, 0,
  ];

  static const defaultDuration = 1.0;
  double duration;

  bool get isWithoutDuration => duration == 0.0;

  /// Used to be able to determine the start state of the component.
  ColorFilterMatrix startColorFilterMatrix;
  double startOpacity;

  /// Used to be able to determine the end state of a sequence of effects.
  ColorFilterMatrix endColorFilterMatrix;
  double endOpacity;

  /// Whether the state of a field was slowly modified by the effect.
  bool get isModifies => isModifiesColorFilterMatrix || isModifiesOpacityMatrix;

  bool get isModifiesColorFilterMatrix =>
      startColorFilterMatrix != endColorFilterMatrix;

  bool get isModifiesOpacityMatrix => startOpacity != endOpacity;

  /// Whether the state of a field was immediately modified by the effect.
  bool get isImmediately =>
      isImmediatelyColorFilterMatrix && isImmediatelyOpacity;

  bool get isImmediatelyColorFilterMatrix =>
      isWithoutDuration || startColorFilterMatrix == endColorFilterMatrix;

  bool get isImmediatelyOpacity =>
      isWithoutDuration || startOpacity == endOpacity;

  SpriteComponentRenderEffect({
    this.duration = defaultDuration,
    Curve? curve,
    this.startColorFilterMatrix = identityColorFilterMatrix,
    this.startOpacity = 1.0,
    this.endColorFilterMatrix = identityColorFilterMatrix,
    this.endOpacity = 1.0,
    VoidCallback? onComplete,
  })  : assert(duration >= 0, 'Duration should be greater or equals 0.'),
        assert(startColorFilterMatrix.length == 5 * 4,
            'Should contains 5 x 4 = 20 elements.'),
        assert(endColorFilterMatrix.length == 5 * 4,
            'Should be 5 x 4 = 20 elements.'),
        super(
          false,
          false,
          isRelative: false,
          curve: curve,
          onComplete: onComplete,
        );

  @mustCallSuper
  @override
  void initialize(SpriteComponent component) {
    super.initialize(component);

    peakTime = duration;
  }

  @mustCallSuper
  @override
  void update(double dt) {
    super.update(dt);

    if (isModifies) {
      this.component?.overridePaint = _overridePaint(curveProgress);
    }
  }

  @mustCallSuper
  @override
  void reset() {
    super.reset();

    if (component != null) {
      initialize(component!);
    }
  }

  @override
  void setComponentToOriginalState() {
    this.component?.overridePaint = _overridePaint(0.0);
  }

  @override
  void setComponentToEndState() {
    this.component?.overridePaint = _overridePaint(1.0);
  }

  Paint _overridePaint(double t) {
    // \todo Add `t` dependencies.

    Fimber.i('t ${t.s3}');

    final paint = Paint();

    if (isImmediatelyColorFilterMatrix) {
      paint.colorFilter = ColorFilter.matrix(endColorFilterMatrix);
      return paint;
    }

    ColorFilterMatrix m = <double>[];
    for (int i = 0; i < startColorFilterMatrix.length; ++i) {
      m.add(startColorFilterMatrix[i] +
          (endColorFilterMatrix[i] - startColorFilterMatrix[i]) * t);
    }
    paint.colorFilter = ColorFilter.matrix(m);

    //paint.color = Color.fromRGBO(0, 0, 0, 0.7);
    //paint.blendMode = BlendMode.srcATop;
    //paint.colorFilter = ColorFilter.mode(Colors.grey, BlendMode.srcATop);

    return paint;
  }
}

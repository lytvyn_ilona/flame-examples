import 'package:flame/effects.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../effects/weffect.dart';
import '../converters/curve_json_converter.dart';
import '../welement.dart';

// Run in the terminal for generate this file:
//   flutter packages pub run build_runner build
// \see https://medium.com/flutter-community/generate-the-code-to-parse-your-json-in-flutter-c68aa89a81d9
part 'scale_in_out.g.dart';

@JsonSerializable(includeIfNull: false)
class ScaleInOut extends WEffect {
  /// in seconds
  final double scale;

  ScaleInOut({
    double duration = 1.0,
    this.scale = 1.0,
    Curve curve = CurveJsonConverter.defaultCurve,
  })  : assert(duration > 0),
        assert(scale > 0),
        super(
          name: 'ScaleInOut',
          duration: duration,
          curve: curve,
        );

  @override
  @mustCallSuper
  void run(Welement we) {
    super.run(we);

    final delta = we.size.length * (1 - scale).abs();
    final speed = delta / duration;
    final effect = ScaleEffect(
      size: we.size * scale,
      speed: speed,
      curve: curve,
      isInfinite: false,
      isAlternating: true,
      onComplete: () => onComplete(we),
    );
    we.addEffect(effect);
  }

  factory ScaleInOut.fromJson(Map<String, dynamic> json) =>
      _$ScaleInOutFromJson(json);

  @mustCallSuper
  @override
  Map<String, dynamic> toJson() =>
      super.toJson()..addAll(_$ScaleInOutToJson(this));
}

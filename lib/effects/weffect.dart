import 'package:fimber/fimber.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/foundation.dart';

import '../converters/curve_json_converter.dart';
import '../extensions/json_extension.dart';
import '../state_machine_data.dart';
import '../welement.dart';

// Run in the terminal for generate this file:
//   flutter packages pub run build_runner build
// \see https://medium.com/flutter-community/generate-the-code-to-parse-your-json-in-flutter-c68aa89a81d9
//part 'weffect.g.dart';

/// \warning All children should be call onComplete() when completed.
/// \warning We should extend `WEffectExtension` after added effect.
abstract class WEffect {
  /// \TODO With final keyword we lost the `name` when serialize to JSON.
  /// See weffect_test/JsonSerializable.
  /// But without final picture_source_test/JsonSerializable is red.
  final String name;

  /// in seconds
  final double duration;

  /// \see Click Ctrl+`Curve` or look at https://youtu.be/qnnlGcZ8vaQ for build an own curve.
  @CurveJsonConverter()
  final Curve curve;

  WEffect({
    required this.name,
    this.duration = 1.0,
    this.curve = CurveJsonConverter.defaultCurve,
  })  : assert(name.length > 0),
        assert(duration >= 0);

  @mustCallSuper
  void run(Welement we) {
    //we.clearEffects();
    Fimber.i('Run effect `$name` for `${we.name}`');
  }

  void onComplete(Welement we) => we.sendActionFsm(OnEndAction());

  @mustCallSuper
  // \see TODO for field `name`.
  Map<String, dynamic> toJson() => {'name': name};

  @override
  String toString() => toJson().sjson;

  @override
  bool operator ==(Object b) => (b is WEffect) && name == b.name;

  @override
  int get hashCode => toJson().hashCode;
}

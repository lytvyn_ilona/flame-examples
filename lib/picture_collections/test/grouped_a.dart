import 'package:flame/components.dart';

import '../picture_collection.dart';
import '../../picture_aspect_ratio.dart';
import '../../picture_helper.dart';
import '../../picture_source.dart';
import '../../welement_source.dart';

/// Example with grouped elements.
class PictureCollectionGroupedA extends PictureCollection {
  static const p = PictureHelper(4096, 3072);

  static Future fromCode(PictureAspectRatio aspectRatio) async =>
      PictureCollection()
        ..data = <PictureSource>[
          PictureSource(
            name: 'test_4096x3072',
            size: p.size,
            aspectRatio: aspectRatio,
            background: S(
              name: 'bg',
            ),
            ws: <S>[
              S(
                name: 'two grouped welements placed by center',
                isVisible: false,
                group: [
                  S(
                    name: '512x512',
                    position: p.center,
                    anchor: Anchor.center,
                  ),
                  S(
                    name: '384x384',
                    position: p.center..x += 256,
                    anchor: Anchor.center,
                  ),
                ],
              ),
              S(
                name: 'three grouped welements placed by bottom',
                //isVisible: false,
                group: [
                  S(
                    name: '512x512',
                    //isVisible: false,
                    position: p.bottomRight,
                    anchor: Anchor.bottomRight,
                  ),
                  S(
                    name: '384x384',
                    //isVisible: false,
                    position: p.bottomRight + Vector2(-512, -512),
                    anchor: Anchor.bottomRight,
                    //scale: 0.5,
                  ),
                  S(
                    name: '4096x384',
                    //isVisible: false,
                    position: p.bottomRight,
                    anchor: Anchor.bottomRight,
                  ),
                ],
              ),
            ],
          ),
        ];
}

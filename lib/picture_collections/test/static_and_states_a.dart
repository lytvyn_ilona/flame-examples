import 'package:flame/components.dart';

import '../picture_collection.dart';
import '../../picture_aspect_ratio.dart';
import '../../picture_helper.dart';
import '../../picture_source.dart';
import '../../state_machine_data.dart';
import '../../welement_source.dart';

/// Example with static sprite elements and states.
class PictureCollectionStaticAndStatesA extends PictureCollection {
  static const p = PictureHelper(4100, 3075);

  static Future fromCode(PictureAspectRatio aspectRatio) async =>
      PictureCollection()
        ..data = <PictureSource>[
          PictureSource(
            name: 'test_static_states',
            size: p.size,
            aspectRatio: aspectRatio,
            background: S(
              name: 'bg_segmentation',
            ),
            ws: <S>[
              S(
                name: 'tree_left',
                //isVisible: false,
                position: p.topLeft,
                anchor: Anchor.topLeft,
                //sm: smTreeLeftOnTap,
                sm: smTreeLeftOnTapAndOnEnd,
              ),
              S(
                name: 'bushes_right',
                //isVisible: false,
                position: p.bottomRight,
                anchor: Anchor.bottomRight,
              ),
            ],
          ),
        ];

  static final smTreeLeftOnTap = StateMachineData(
    transitions: [
      const WTransition('spring', WActions.onTap, 'summer'),
      const WTransition('summer', WActions.onTap, 'autumn'),
      const WTransition('autumn', WActions.onTap, 'winter'),
      const WTransition('winter', WActions.onTap, 'spring'),
    ],
  );

  static final smTreeLeftOnTapAndOnEnd = StateMachineData(
    transitions: [
      const WTransition('spring', WActions.onTap, 'summer'),
      const WTransition('summer', WActions.onEnd, 'autumn'),
      const WTransition('autumn', WActions.onEnd, 'winter'),
      const WTransition('winter', WActions.onEnd, 'spring'),
    ],
  );
}

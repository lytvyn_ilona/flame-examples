import '../picture_aspect_ratio.dart';
import '../picture_source.dart';

class PictureCollection<T extends PictureSource> {
  List<T> data = [];
}

/// A load files from JSON-assets.
class PictureCollectionJson extends PictureCollection {
  static Future fromAssets(
    List<String> pictures,
    PictureAspectRatio aspectRatio,
  ) async {
    final o = PictureCollection();
    //pictures.forEach((name) async { - Don't do inner async forEach!
    for (final String name in pictures) {
      final ps = await PictureSource.fromAsset(name, aspectRatio);
      if (ps != null) {
        o.data.add(ps);
      }
    }

    return o;
  }
}

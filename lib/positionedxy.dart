import 'package:flutter/material.dart';

class PositionedXY extends StatelessWidget {
  final Widget child;
  final dChild;
  final x;
  final y;
  final widthPicture;
  final heightPicture;

  PositionedXY({
    required this.child,
    required this.dChild,
    required this.x,
    required this.y,
    required this.widthPicture,
    required this.heightPicture,
  });

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: x,
      top: y,
      child: child,
    );
  }
}

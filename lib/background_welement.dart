import 'dart:ui';

import 'package:fimber/fimber.dart';
import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flutter/foundation.dart';

import 'extensions/string_extension.dart';
import 'extensions/vector_extension.dart';
import 'welement.dart';
import 'welement_source.dart';

class BackgroundWelement extends Welement implements SpriteComponent {
  static const allowedFileExtension = 'webp';

  Sprite? sprite;

  /// Use this to override the colour used (to apply tint or opacity).
  /// If not provided the default is `Sprite.paint`.
  Paint? overridePaint;

  BackgroundWelement({
    required String canvasName,
    required Vector2 canvasSize,
    required double globalScale,
    required WelementSource welementSource,
    required Vector2 screenSize,
  }) : super(
          canvasName: canvasName,
          canvasSize: canvasSize,
          globalScale: globalScale,
          welementSource: welementSource,
          screenSize: screenSize,
        );

  @override
  Future<void> onLoad() async {
    position
        .setFrom(welementSource.position.toVector2DependsOfSize(canvasSize));
    anchor = Anchor.topLeft;
    sprite = await loadSprite();
    if (sprite != null) {
      size.setFrom(sprite!.srcSize * ownScale);
    }

    Fimber.i('background position ${position.s1} size ${size.s1}');
    Fimber.i('screen size ${screenSize.s1}');
  }

  Future<Sprite?> loadSprite() async {
    final filename = pathSprite;
    Fimber.i('Look at `$filename`...');
    try {
      return await gameRef.loadSprite(filename);
    } catch (ex) {
      // skip, return a null below
    }

    Fimber.e('File `$filename` is not found.');
    return null;
  }

  @mustCallSuper
  @override
  void render(Canvas canvas) {
    super.render(canvas);
    sprite?.render(
      canvas,
      size: size,
      overridePaint: overridePaint,
    );
  }

  /// Returns path to sprite with extension.
  String get pathSprite => '$canvasName/$name.$allowedFileExtension';

  @override
  Map<String, dynamic> toJson() => super.toJson()
    ..addAll({
      'nice scale': globalScale.n1,
    });
}

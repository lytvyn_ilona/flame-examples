import 'dart:async';
import 'dart:ui';

import 'package:flame/flame.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fimber/fimber.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sizer/flutter_sizer.dart';

import 'audio.dart';
import 'extensions/json_extension.dart';
import 'picture_aspect_ratio.dart';
import 'picture_canvas.dart';
import 'picture_collections/picture_collection.dart';
import 'picture_collections/test/animated_a.dart';
import 'picture_collections/test/flat_a.dart';
import 'picture_collections/test/grouped_a.dart';
import 'picture_collections/test/static_and_states_a.dart';
import 'picture_source.dart';
import 'service_locator.dart';

void main() async {
  Fimber.plantTree(DebugTree.elapsed(useColors: true));

  WidgetsFlutterBinding.ensureInitialized();

  await prepareApp();

  Flame.assets.clearCache();

  SystemChrome.setPreferredOrientations([
    // \todo Enable portrait orientation.
    //DeviceOrientation.portraitUp,
    //DeviceOrientation.portraitDown,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]).then((_) {
    runApp(App());
  });
}

Future<void> prepareApp() async {
  await Flame.device.fullScreen();
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  Audio get audio => sl.get<Audio>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);

    // sometimes a first init in the main() doesn't work
    prepareApp();

    initServiceLocator(context);
  }

  @override
  void dispose() {
    audio.release();
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    Fimber.i('App state is `$state`');
    switch (state) {
      case AppLifecycleState.resumed:
        _resumeCurrentActions();
        break;
      case AppLifecycleState.paused:
        _pauseCurrentActions();
        break;
      default:
        break;
    }
  }

  @override
  Future<bool> didPopRoute() async {
    _pauseCurrentActions();
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return FlutterSizer(
      builder: (context, orientation, screenType) {
        Fimber.i('Device orientation in widget App by Sizer $orientation');
        Fimber.i('About device:'
            '\n\torientation $orientation'
            '\n\tscreen type $screenType'
            '\n\tpixel ratio ${Device.devicePixelRatio}'
            '\n\tboxConstraints ${Device.boxConstraints}');
        return MaterialApp(
          title: 'Happiness',
          home: Scaffold(
            body: Package(title: ''),
          ),
          theme: ThemeData(
            primarySwatch: Colors.yellow,
          ),
          debugShowCheckedModeBanner: false,
        );
      },
    );
  }

  void _pauseCurrentActions() {
    audio.pause();
  }

  void _resumeCurrentActions() {
    audio.resume();
  }
}

class Package extends StatefulWidget {
  final String title;

  Package({Key? key, required this.title}) : super(key: key);

  @override
  _PackageState createState() => _PackageState();
}

class _PackageState extends State<Package> {
  _PackageState();

  @override
  Widget build(BuildContext context) {
    return kDebugMode ? _buildStartPage() : _buildWithSplash();
  }

  Widget _buildWithSplash() {
    // \todo Add splashscreen with null-safety support.
    return _buildStartPage();
  }

  Widget _buildStartPage() {
    return Happiness();
  }
}

class Happiness extends StatefulWidget {
  @override
  _HappinessState createState() => _HappinessState();
}

class _HappinessState extends State<Happiness> {
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Fimber.i(
        'Device orientation in widget Happiness by MediaQuery ${MediaQuery.of(context).orientation}');

    final aspectRatio = PictureAspectRatio(context);

    const pictures = <String>[
      // === tests ===
      //'test_4096x3072',
      //'test_grey_to_color_16to9',

      // === landscape ===
      'fairy',
      'fairy_tale_in_the_wood',
      'fox_and_fish',
      'girl_and_deer',
      'girl_and_whale',
      'spring_girl',
      //'winter_apple',

      // === portrait ===
    ];

    final collection = PictureCollectionJson.fromAssets(pictures, aspectRatio);

    //final collection = PictureCollectionFlatA.fromCode(aspectRatio);
    //final collection = PictureCollectionGroupedA.fromCode(aspectRatio);
    //final collection = PictureCollectionAnimatedA.fromCode(aspectRatio);
    //final collection = PictureCollectionStaticAndStatesA.fromCode(aspectRatio);

    return FutureBuilder(
      future: collection,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Text('Loading...');
          default:
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            }
            return snapshot.hasData
                ? _buildPage(snapshot.data as PictureCollection)
                : Text('Init...');
        }
      },
    );
  }

  Widget _buildPage(PictureCollection pictureCollection) {
    final d = pictureCollection.data;

    final widgetList = <Widget>[];
    d.forEach((ps) => widgetList.add(PictureCanvas(pictureSource: ps)));

    // \test Show picture source like JSON.
    final s = d.first.toString();
    Fimber.i(s);

    // \test
    //widgetList.addAll(_createTestPages(widgetList.length));

    //_updateDeviceOrientation(d.first);

    return PageView(
      controller: _pageController,
      children: widgetList,
      onPageChanged: (i) {
        //final pc = widgetList[i] as PictureCanvas;
        //_updateDeviceOrientation(pc.pictureSource);
      },
    );
  }

  void _updateDeviceOrientation(PictureSource pictureSource) {
    Fimber.i(
        'Set device orientation to ${pictureSource.isPortrait ? 'portrait' : 'landscape'}');
    pictureSource.isPortrait
        ? Flame.device.setPortrait()
        : Flame.device.setLandscape();
  }

  List<Widget> _createTestPages(int alreadyCreatedPages) {
    return <Widget>[
      Container(
        color: Colors.red,
        child: Center(
          child: ElevatedButton(
            onPressed: () {
              if (_pageController.hasClients) {
                _pageController.animateToPage(
                  alreadyCreatedPages + 1,
                  duration: const Duration(milliseconds: 1500),
                  curve: Curves.easeOutSine,
                );
              }
            },
            child: Text('Next'),
          ),
        ),
      ),
      Container(
        color: Colors.blue,
        child: Center(
          child: ElevatedButton(
            onPressed: () {
              if (_pageController.hasClients) {
                _pageController.animateToPage(
                  alreadyCreatedPages,
                  duration: const Duration(milliseconds: 1500),
                  curve: Curves.easeInSine,
                );
              }
            },
            child: Text('Previous'),
          ),
        ),
      ),
    ];
  }
}

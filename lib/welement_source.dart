import 'package:flame/components.dart';
import 'package:json_annotation/json_annotation.dart';

import 'animation_welement.dart';
import 'converters/anchor_json_converter.dart';
import 'extensions/anchor_extension.dart';
import 'extensions/json_extension.dart';
import 'extensions/vector_extension.dart';
import 'service_locator.dart';
import 'state_machine_data.dart';
import 'welement_order_counter.dart';

// Run in the terminal for generate this file:
//   flutter packages pub run build_runner build
// \see https://medium.com/flutter-community/generate-the-code-to-parse-your-json-in-flutter-c68aa89a81d9
part 'welement_source.g.dart';

@JsonSerializable(includeIfNull: false, ignoreUnannotated: true)
class WelementSource {
  static const defaultPosition = '0 0';
  static const defaultAnchor = AnchorJsonConverter.defaultAnchor;

  /// Introduced it for easy management.
  static final defaultStateMachineData = StateMachineData(
    transitions: [
      const WTransition('idle', WActions.onTap, 'idle'),
    ],
  );

  @JsonKey()
  final String name;

  bool get isBackground => name == 'background';

  @JsonKey()
  final bool isPartBackground;

  /// Like String because can define a coords depends a picture size.
  /// \see Test 'demo picture recovered from JSON data depends a picture size'.
  @JsonKey()
  final String position;

  @JsonKey()
  @AnchorJsonConverter()
  final Anchor anchor;

  @JsonKey()
  final StateMachineData sm;

  @JsonKey()
  final List<WelementSource> group;

  @JsonKey()
  final bool isVisible;

  /// Play or not play a default sound by sprite name when sound effect
  /// is not placed.
  @JsonKey()
  final bool isMute;

  /// We can make the welement bigger or smaller.
  @JsonKey()
  final double scale;

  bool get hasStateMachine => sm.transitionList.isNotEmpty;

  bool get isGroup => group.isNotEmpty;

  Future<bool> isAnimation(String canvasName) async =>
      AnimationWelement.isAnimation(canvasName, name);

  /// For catching the taps by welements.
  int order = -1;

  int get _nextCount => sl.get<WelementOrderCounter>().next();

  WelementSource({
    required this.name,
    bool? isPartBackground,
    dynamic position,
    required this.anchor,
    StateMachineData? sm,
    List<WelementSource>? group,
    bool? isVisible,
    bool? isMute,
    double? scale,
  })  : assert(name.length > 0),
        assert(scale == null || scale > 0),
        isPartBackground = isPartBackground ?? false,
        position = _preparePosition(position),
        sm = sm ?? defaultStateMachineData,
        group = group ?? <WelementSource>[],
        isVisible = isVisible ?? true,
        isMute = isMute ?? false,
        scale = scale ?? 1.0 {
    order = _nextCount;
  }

  factory WelementSource.fromString(String s) =>
      WelementSource.fromJson(s.json);

  factory WelementSource.fromJson(Map<String, dynamic> json) =>
      _$WelementSourceFromJson(json);

  Map<String, dynamic> toJson() => _$WelementSourceToJson(this);

  @override
  String toString() => toJson().sjson;

  @override
  bool operator ==(Object b) =>
      (b is WelementSource) &&
      name == b.name &&
      // \todo we are interested the real position
      position.toVector2DependsOfSize(Vector2.zero()) ==
          b.position.toVector2DependsOfSize(Vector2.zero()) &&
      anchor.s == b.anchor.s;

  @override
  int get hashCode => toJson().hashCode;

  static String _preparePosition(dynamic pd) {
    if (pd is String) {
      final s = pd.trim();
      return s.isEmpty ? defaultPosition : s;
    }

    if (pd is List || pd is Vector2) {
      return '${pd[0]} ${pd[1]}';
    }

    return defaultPosition;
  }
}

class S extends WelementSource {
  S({
    required String name,
    bool? isPartBackground,
    dynamic position,
    Anchor? anchor,
    StateMachineData? sm,
    List<WelementSource>? group,
    bool? isVisible,
    bool? isMute,
    double? scale,
  }) : super(
          name: name,
          isPartBackground: isPartBackground,
          position: position,
          anchor: anchor ?? Anchor.center,
          sm: sm,
          group: group,
          isVisible: isVisible,
          isMute: isMute,
          scale: scale,
        );
}

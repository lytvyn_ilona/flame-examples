import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:collection/collection.dart';
import 'package:fimber/fimber.dart';
import 'package:flutter/foundation.dart';

import 'extensions/asset_extension.dart';

void _audioPlayerHandler(PlayerState value) => print('state => $value');

class Audio {
  static const basePath = 'audio';
  static const defaultVolume = 1.0;

  Audio() {
    //AudioPlayer.logEnabled = kDebugMode;
  }

  final AudioPlayer _player = AudioPlayer(mode: PlayerMode.LOW_LATENCY);
  final AudioCache _cache = AudioCache();

  final playDataList = PriorityQueue<PlayData>();

  Future<void> play(PlayData playData) async {
    if (!kIsWeb && Platform.isIOS) {
      // just example for correct callback on iOS
      // \thanks https://stackoverflow.com/a/61965965/963948
      _player.onPlayerStateChanged.listen(_audioPlayerHandler);
    }

    final filename = '$basePath/${playData.filename}';
    if (!await playData.filename.isAssetAudioExists) {
      Fimber.i('The sprite `${playData.welementName}`'
          ' on the picture `${playData.canvasName}` without sound.'
          ' Look at file `$filename`.');
      return;
    }

    // !) not available for Flutter Web
    Fimber.i('Play the file `$filename`...');
    _cache.play(filename, volume: playData.volume).catchError((error) {
      Fimber.w('Error: $error');
    });
  }

  void pause() {
    _player.pause();
  }

  void stop() {
    _player.stop();
  }

  void release() {
    _player.release();
  }

  void resume() {
    _player.resume();
  }
}

class PlayData implements Comparable<PlayData> {
  final int order;
  final String canvasName;
  final String welementName;
  final double volume;

  String get filename => '$canvasName/${welementName}_1.mp3';

  const PlayData({
    this.order = -1,
    required this.canvasName,
    required this.welementName,
    this.volume = Audio.defaultVolume,
  })  : assert(order == -1 || order > 0),
        assert(canvasName.length > 0),
        assert(welementName.length > 0),
        assert(volume > 0);

  int compareTo(PlayData other) {
    return other.order - order;
  }
}

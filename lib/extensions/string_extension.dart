import 'dart:math';

extension DoubleStringExtension on double {
  double get n1 => np(1);

  double get n2 => np(2);

  double get n3 => np(3);

  double np(int digits) {
    final p = pow(10, digits);
    return (this * p).roundToDouble() / p;
  }

  String get s1 => toStringAsFixed(1);

  String get s2 => toStringAsFixed(2);

  String get s3 => toStringAsFixed(3);
}

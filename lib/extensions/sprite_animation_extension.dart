import 'package:fimber/fimber.dart';
import 'package:flame/components.dart';
import 'package:flame/flame.dart';

extension SpriteAnimationExtension on SpriteAnimation {
  static Future<SpriteAnimation> fromFolderWithAtlas(
    String pathAtlas,
    String pathData,
  ) async {
    final image = await Flame.images.load(pathAtlas);
    final data = await Flame.assets.readJson(pathData);
    Fimber.i('data from `$pathData`:\n$data');
    return SpriteAnimation.fromAsepriteData(image, data);
  }
}

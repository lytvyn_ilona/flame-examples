import 'dart:ui';

import 'package:fimber/fimber.dart';
import 'package:flame/components.dart';

import '../extensions/color_extension.dart';

extension SpriteExtension on Sprite {
  /// \param lowLimitColorAlpha Define tapping when an alpha into a sprite above this value.
  Future<bool> isOpacityPixel(
    Vector2 pos, {
    double scale = 1.0,
    lowLimitColorAlpha = 0x30,
  }) async {
    final int scaledX = (pos.x / scale).round();
    final int scaledY = (pos.y / scale).round();
    final bytes = await image.toByteData(format: ImageByteFormat.rawRgba);
    final int width = srcSize.x.round();
    final int height = srcSize.y.round();
    final byteOffset = 4 * (scaledX + (scaledY * width));
    if (byteOffset >= 4 * width * height || byteOffset < 0) {
      Fimber.w('$pos are out of range for sprite $srcSize');
      return false;
    }

    final color = bytes!.colorAtByteOffset(byteOffset);
    return color.alpha > lowLimitColorAlpha;
  }
}

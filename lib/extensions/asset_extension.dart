import 'package:flutter/services.dart' show rootBundle;

extension AssetExtension on String {
  Future<bool> get isAssetExists async {
    return await isAssetRootExists ||
        await isAssetImagesExists ||
        await isAssetAudioExists;
  }

  Future<bool> get isAssetRootExists async {
    try {
      await rootBundle.load('assets/$this');
    } catch (ex) {
      return false;
    }
    return true;
  }

  Future<bool> get isAssetImagesExists async {
    try {
      await rootBundle.load('assets/images/$this');
    } catch (ex) {
      return false;
    }
    return true;
  }

  Future<bool> get isAssetAudioExists async {
    try {
      await rootBundle.load('assets/audio/$this');
    } catch (ex) {
      return false;
    }
    return true;
  }
}

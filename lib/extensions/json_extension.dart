import 'dart:convert' as convert;
import 'dart:ui';

import 'package:flame/components.dart';

import '../picture_aspect_ratio.dart';
import '../extensions/device_extension.dart';

final _encoder = convert.JsonEncoder.withIndent('  ');

extension PictureAspectRatioJsonExtension on PictureAspectRatio {
  Map<String, dynamic> get json => {
        'screen size': screenSize().json,
        'screen orientation': screenOrientation().sd,
      };

  String get sjson => json.toString();
}

extension SizeJsonExtension on Size {
  List<int> get json => [width.round(), height.round()];

  String get sjson => '[${width.round()}, ${height.round()}]';
}

extension Vector2JsonExtension on Vector2 {
  List<double> get json => [x, y];

  String get sjson => '[$x, $y]';
}

extension ObjectJsonExtension on Object {
  String get sjson => _encoder.convert(this);
}

extension StringJsonExtension on String {
  Map<String, dynamic> get json => convert.json.decode(this);
}

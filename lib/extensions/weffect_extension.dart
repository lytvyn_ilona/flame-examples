import 'package:collection/collection.dart' show IterableExtension;

import '../effects/color_to_grey.dart';
import '../effects/grey_to_color.dart';
import '../effects/play_sound_by_tap.dart';
import '../effects/scale_in_out.dart';
import '../effects/weffect.dart';

extension WEffectExtension on WEffect {
  static final prototypeList = <String, dynamic>{
    'ColorToGrey': (Map<String, dynamic> d) => ColorToGrey.fromJson(d),
    'GreyToColor': (Map<String, dynamic> d) => GreyToColor.fromJson(d),
    'PlaySoundByTap': (Map<String, dynamic> d) => PlaySoundByTap.fromJson(d),
    'ScaleInOut': (Map<String, dynamic> d) => ScaleInOut.fromJson(d),
  };

  String get s =>
      prototypeList.keys.firstWhereOrNull(
        (key) => prototypeList[key] == this,
      ) ??
      '';
}

extension WEffectBuildStringExtension on Map<String, dynamic> {
  WEffect get buildEffect {
    assert(this.isEffect);
    final name = this.keys.first;
    final prototypeEffect = WEffectExtension.prototypeList[name];
    final params = this.values.first;
    return prototypeEffect(params);
  }

  bool get isEffect {
    final name = this.keys.first;
    return WEffectExtension.prototypeList.keys.contains(name);
  }
}

import 'dart:typed_data';
import 'dart:ui';

extension ColorByteDataExtension on ByteData {
  Color colorAtByteOffset(int offset) => Color(getUint32(offset).rgbaToArgb());
}

extension ColorIntExtension on int {
  int rgbaToArgb() {
    final a = this & 0xFF;
    final rgb = this >> 8;
    return rgb + (a << 24);
  }
}

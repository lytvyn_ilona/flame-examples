import 'package:fimber/fimber.dart';
import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
//import 'package:flame/gestures.dart';
import 'package:flame/src/gestures/events.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/painting.dart';

import 'animation_welement.dart';
import 'background_welement.dart';
import 'group_welement.dart';
import 'levelers/leveler.dart';
import 'levelers/remove-invisible-action-leveler.dart';
import 'picture_game.dart';
import 'fsm.dart';
import 'extensions/anchor_extension.dart';
import 'extensions/json_extension.dart';
import 'extensions/string_extension.dart';
import 'extensions/vector_extension.dart';
import 'resolver.dart';
import 'service_locator.dart';
import 'sprite_welement.dart';
import 'state_machine_data.dart';
import 'welement_source.dart';

abstract class Welement extends PositionComponent with HasGameRef<PictureGame> {
  final String canvasName;

  final Vector2 canvasSize;

  double get ownScale => welementSource.scale;

  double localScale = 1.0;

  final double globalScale;

  final WelementSource welementSource;

  final Vector2 screenSize;

  Vector2 get unscaledScreenSize => screenSize / globalScale;

  Rect get unscaledScreenRect => unscaledScreenSize.toRect();

  Vector2 get scaledSize => size * globalScale * localScale;

  Vector2 get unscaledSize => size / globalScale / localScale;

  String get name => welementSource.name;

  int get order => welementSource.order;

  bool get isBackground => welementSource.isBackground;

  bool get isPartBackground => welementSource.isPartBackground;

  bool get isVisible => _isVisibleOverride ?? welementSource.isVisible;

  bool? _isVisibleOverride;

  set isVisible(bool v) => _isVisibleOverride = v;

  bool get isMute => welementSource.isMute;

  final GroupWelement? group;

  bool get isGroup => group != null;

  late Leveler _leveler;

  /// Finite state machine for animations, effects, etc.
  /// \see sendActionFsm()
  late FSM fsm;

  String get stateName => fsm.stateName ?? '';

  /// We can't transfer the gestures to effects through WAction
  /// therefore save them in the welement itself.
  TapDownInfo? lastTapDownDetails;

  Resolver get resolver => sl.get<Resolver>();

  Welement({
    required this.canvasName,
    required this.canvasSize,
    required this.globalScale,
    required this.welementSource,
    required this.screenSize,
    this.group,
  })  : assert(canvasName.length > 0),
        assert(screenSize.length2 > 0),
        assert(canvasSize.length2 > 0),
        assert(globalScale > 0) {
    _initLeveler();
    _initFsm();
  }

  void _initLeveler() {
    Fimber.i('Init the Leveler for `$name`');
    _leveler = Leveler(welement: this);

    final backgroundList = <ActionLeveler>[];
    sendActionLevelerList<BackgroundWelement>(backgroundList);
    if (isPartBackground) {
      sendActionLevelerList<BackgroundWelement>(backgroundList);
    }

    final spriteAndAnimationList = <ActionLeveler>[
      //ScaleActionLeveler(),
      //PositionIterationActionLeveler(),
      //PositionActionLeveler(),
      RemoveInvisibleActionLeveler(),
    ];
    sendActionLevelerList<SpriteWelement>(spriteAndAnimationList);
    sendActionLevelerList<AnimationWelement>(spriteAndAnimationList);
  }

  void _initFsm() {
    if (!welementSource.hasStateMachine) {
      return;
    }

    Fimber.i('Init the finite state machine for `$name`');
    fsm = FSM();

    // start state will be the first state from the `stateList`
    //final startState = fsm!.newStartState;
    welementSource.sm.transitionList.forEach((WTransition state) {
      Fimber.i(
          'Add transition ${state.from} -> ${state.action} -> ${state.to}');
      fsm.addTransition(state.from, state.action, state.to);
    });
    //final stopState = fsm!.newStopState;

    welementSource.sm.directorList
        .forEach((WDirector director) => fsm.addCallbacks(
              director.stateName,
              onEntry: () => director.runAll(this),
            ));

    fsm.start();
  }

  void sendActionLeveler<T>(ActionLeveler actionLeveler) {
    sendActionLevelerList([actionLeveler]);
  }

  void sendActionLevelerList<T>(List<ActionLeveler> actionLevelerList) {
    if (actionLevelerList.isNotEmpty && this is T) {
      Fimber.i('Add $actionLevelerList for `$name`');
      _leveler.actionLevelerList.addAll(actionLevelerList);
    }
  }

  void processScreenAlignment(double dt) {
    if (_leveler.isNotEmpty && size.length2 > 0) {
      _leveler.process(dt);
    }
  }

  @override
  bool containsPoint(Vector2 point) {
    return super.containsPoint(point / globalScale);
  }

  bool sendActionFsm(WAction action) {
    final prevState = fsm.current;
    fsm.sendAction(action);
    if (prevState != fsm.current) {
      onStateUpdated();
    }
    return true;
  }

  @mustCallSuper
  void onStateUpdated() {
    Fimber.i('FSM state for `$name` updated to `$stateName`');
  }

  @mustCallSuper
  @override
  void update(double dt) {
    super.update(dt);
    // run effects for top welement which was pushed early, see onTapDown()
    resolver.runTopPushed(dt);
  }

  /// How part of element we will see at the screen.
  /// In percents, [0; 100].
  int viewedPercent() {
    if (PictureGame.showWholePicture) {
      return 100;
    }

    if (!isVisible) {
      return 0;
    }

    final weRect = toRect();
    final intersectRect = weRect.intersect(unscaledScreenRect);

    final isOverlapped = intersectRect.width > 0 && intersectRect.height > 0;
    if (!isOverlapped) {
      return 0;
    }

    final overlappedSquare = intersectRect.width * intersectRect.height;
    final square = weRect.width * weRect.height;

    return (overlappedSquare / square * 100).round();
  }

  Map<String, dynamic> toJson() {
    final r = <String, dynamic>{
      'canvas name': canvasName,
      'name': name,
      'canvas size': canvasSize.toJson1(),
      'order': order,
      'position': position.toJson1(),
      'anchor': anchor.s,
      'size': size.toJson1(),
      'own scale': ownScale.n1,
      'local scale': localScale.n1,
      'global scale': globalScale.n1,
      'screen size': screenSize.toJson1(),
      'fsm': fsm.toString(),
    };
    if (group != null) {
      r.addAll({
        'group name': group!.name,
        'group count': group!.count,
      });
    }
    if (isPartBackground == true) {
      r.addAll({
        'is part background': isPartBackground,
      });
    }
    if (isVisible == false) {
      r.addAll({
        'is visible': isVisible,
      });
    }
    if (isMute == true) {
      r.addAll({
        'is mute': isMute,
      });
    }
    return r;
  }

  @override
  String toString() => toJson().sjson;
}

import 'dart:collection';

import 'package:fimber/fimber.dart';
import 'package:flutter/widgets.dart';

import 'extensions/device_extension.dart';

/// \see https://en.wikipedia.org/wiki/Aspect_ratio_(image)
class PictureAspectRatio {
  /// Always use pairs as landscape AND portrait orientations.
  /// Most preferred go first. See the reason in PictureSource.buildPathData().
  static const aspectSizeList = <Size>[
    Size(16, 9),
    Size(9, 16),
    Size(16, 7),
    Size(7, 16),
    Size(4, 3),
    Size(3, 4),
    Size(1, 1),
  ];

  final BuildContext context;

  const PictureAspectRatio(this.context);

  /*
  Size screenSize() => (screenOrientation().isLandscape &&
          MediaQuery.of(context).size.width >
              MediaQuery.of(context).size.height)
      ? MediaQuery.of(context).size
      : MediaQuery.of(context).size.swapped;
  */
  //Size screenSize() =>
  //    _isCorrectContext() ? MediaQuery.of(context).size : Size.zero;
  Size screenSize() {
    if (!_isCorrectContext()) {
      return Size.zero;
    }

    final ss = MediaQuery.of(context).size;
    Fimber.i('screenSize() is $ss');

    if (ss.width < ss.height && isScreenPortrait()) {
      return ss;
    }

    if (ss.width >= ss.height && isScreenLandscape()) {
      return ss;
    }

    if (ss.width < ss.height && isScreenLandscape()) {
      return ss.swapped;
    }

    return ss;
  }

  Orientation screenOrientation() {
    if (!_isCorrectContext()) {
      return Orientation.portrait;
    }

    return MediaQuery.of(context).orientation;
  }

  static Orientation orientation(Size size) =>
      (size.width > size.height) ? Orientation.landscape : Orientation.portrait;

  bool isScreenLandscape() => screenOrientation().isLandscape;

  bool isScreenPortrait() => screenOrientation().isPortrait;

  static bool isLandscape(Size size) => orientation(size).isLandscape;

  static bool isPortrait(Size size) => orientation(size).isPortrait;

  double screenAspect() => aspect(screenSize());

  static double aspect(Size size) => size.aspectRatio;

  Size preferredScreenAspectSize() => preferredAspectSize(screenSize());

  // \see picture_aspect_ratio_test.dart
  static Size preferredAspectSize(Size size) {
    final map = SplayTreeMap<double, Size>();
    aspectSizeList.forEach((aspectSize) {
      map[_deltaAspectRatio(size, aspectSize)] = aspectSize;
    });

    return map.values.first;
  }

  String preferredScreenSuffix() =>
      preferredScreenAspectSize().likeAspectRatioSuffix;

  static String preferredSuffix(Size aspectSize) =>
      aspectSize.likeAspectRatioSuffix;

  static double _deltaAspectRatio(Size a, Size b) =>
      (a.aspectRatio - b.aspectRatio).abs();

  // For unit tests.
  // \see debugCheckHasMediaQuery()
  bool _isCorrectContext() => (context.widget is MediaQuery ||
      context.findAncestorWidgetOfExactType<MediaQuery>() != null);
}

extension PictureAspectRatioSizeExtension on Size {
  static const suffixDelimiter = 'to';

  String get likeAspectRatioSuffix =>
      '${this.width.round()}$suffixDelimiter${this.height.round()}';

  Size get swapped => Size(this.height, this.width);
}

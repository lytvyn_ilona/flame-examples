import 'dart:math';

import 'package:flame/components.dart';

class Utils {
  /// Returns a `value` which normalized to range [a, b].
  /// \see scaleToRangeVector2()
  static double scaleToRange(
    double value,
    double minValue,
    double maxValue,
    double a,
    double b,
  ) {
    final num v = value.clamp(minValue, maxValue);
    return (b - a) * (v - minValue) / (maxValue - minValue) + a;
  }

  /// Same scaleToRange() by Vector2 values.
  static Vector2 scaleToRangeVector2(
    Vector2 value,
    Vector2 minValue,
    Vector2 maxValue,
    Vector2 a,
    Vector2 b,
  ) {
    return Vector2(
        Utils.scaleToRange(value.x, minValue.x, maxValue.x, a.x, b.x),
        Utils.scaleToRange(value.y, minValue.y, maxValue.y, a.y, b.y));
  }

  static Vector2 fitSize(Vector2 someSize, Vector2 screenSize) {
    final rs = screenSize.x / screenSize.y;
    final ri = someSize.x / someSize.y;
    return rs > ri
        ? Vector2(someSize.x * screenSize.y / someSize.y, screenSize.y)
        : Vector2(screenSize.x, someSize.y * screenSize.x / someSize.x);
  }

  static double scaleFitSize(Vector2 someSize, Vector2 screenSize) {
    final size = fitSize(someSize, screenSize);
    return size.x / someSize.x;
  }

  static double scaleCoverSize(Vector2 someSize, Vector2 screenSize) {
    final scaleWidth = (screenSize.x > 0) ? screenSize.x / someSize.x : 1.0;
    final scaleHeight = (screenSize.y > 0) ? screenSize.y / someSize.y : 1.0;
    return max(scaleWidth, scaleHeight);
  }

  static bool isScaleCoverByHeight(Vector2 someSize, Vector2 screenSize) {
    final scaleWidth = (screenSize.x > 0) ? screenSize.x / someSize.x : 1.0;
    final scaleHeight = (screenSize.y > 0) ? screenSize.y / someSize.y : 1.0;
    return scaleWidth < scaleHeight;
  }

  static bool isScaleCoverByWidth(Vector2 someSize, Vector2 screenSize) =>
      !isScaleCoverByHeight(someSize, screenSize);

  static Vector2 correctTooBigSize(Vector2 size, Vector2 screenSize) {
    final kx = size.x / screenSize.x;
    final ky = size.y / screenSize.y;
    final k = max(kx, ky);
    return (k > 1) ? (size / k) : size;
  }

  static Vector2 correctTooSmallSize(Vector2 size, Vector2 screenSize) {
    // \todo Calculate by real visible size.
    // \todo Move all constant to the class Configure.
    const double minSpriteSize = 12;
    double kx = 1;
    if (size.x < minSpriteSize) {
      kx = size.x / minSpriteSize;
    }
    double ky = 1;
    if (size.y < minSpriteSize) {
      ky = size.y / minSpriteSize;
    }
    final k = min(kx, ky);
    return (k < 1) ? (size / k) : size;
  }
}

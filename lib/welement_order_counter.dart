class WelementOrderCounter {
  static int _orderCounter = 0;

  int next() => ++_orderCounter;
}

import 'dart:ui';

import 'package:fimber/fimber.dart';
import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flame/extensions.dart';
//import 'package:flame/gestures.dart';
import 'package:flame/src/gestures/events.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';

import 'group_welement.dart';
import 'extensions/asset_extension.dart';
import 'extensions/sprite_extension.dart';
import 'extensions/string_extension.dart';
import 'extensions/vector_extension.dart';
import 'state_machine_data.dart';
import 'welement.dart';
import 'welement_source.dart';

class SpriteWelement extends Welement with Tappable implements SpriteComponent {
  static const dataFileName = 'sprite.json';
  static const allowedFileExtension = 'webp';

  /// In seconds.
  static const defaultDurationBeforeEnd = 1.0;

  Sprite? sprite;

  /// Use this to override the colour used (to apply tint or opacity).
  /// If not provided the default is `Sprite.paint`.
  Paint? overridePaint;

  /// Time for FSM. Setup it in the `dataFileName` like `duration`.
  double durationBeforeEnd = defaultDurationBeforeEnd;

  /// Set in afterLoad().
  double coverScale = 1.0;
  double fitScale = 1.0;

  Vector2 get coverSize => size * coverScale;

  SpriteWelement({
    required String canvasName,
    required Vector2 canvasSize,
    required double globalScale,
    required WelementSource welementSource,
    required Vector2 screenSize,
    GroupWelement? group,
    this.overridePaint,
  }) : super(
          canvasName: canvasName,
          canvasSize: canvasSize,
          globalScale: globalScale,
          welementSource: welementSource,
          screenSize: screenSize,
          group: group,
        );

  @override
  void onMount() async {
    super.onMount();

    position
        .setFrom(welementSource.position.toVector2DependsOfSize(canvasSize));
    anchor = welementSource.anchor;
    _initSprite();
  }

  void _initSprite() async {
    sprite = await loadSprite();
    if (sprite != null) {
      size.setFrom(sprite!.srcSize * ownScale);
    }

    if (await isPathDataExists) {
      // this sprite with data file, parse it
      final data = await Flame.assets.readJson(pathData);
      Fimber.i('data from `$pathData`:\n$data');
      final duration = data['duration'];
      durationBeforeEnd = (duration is num)
          ? (data['duration'] as num).toDouble()
          : defaultDurationBeforeEnd;
    }

    Fimber.i('sprite'
        '\n\tstate $stateName'
        '\n\tposition $position'
        '\n\tsize $size'
        '\n\tduration $durationBeforeEnd');
  }

  Future<Sprite?> loadSprite() async {
    final filename = await pathSprite;
    Fimber.i('Look at sprite `$filename`...');
    try {
      return await gameRef.loadSprite(filename);
    } catch (ex) {
      // skip, return a null below
    }

    Fimber.e('File `$filename` is not found.');
    return null;
  }

  @mustCallSuper
  @override
  void onStateUpdated() {
    super.onStateUpdated();
    _initSprite();
  }

  @override
  bool onTapDown(TapDownInfo event) {
    Fimber.i('Tap to sprite $this'
        '\nwith global ${event.eventPosition}'
        '\tlocal ${event.eventPosition}');

    // see notes for this property
    lastTapDownDetails = event;

    _pushEffectsWhenSpriteIsOpacity(event);

    return true;
  }

  Future<void> _pushEffectsWhenSpriteIsOpacity(TapDownInfo event) async {
    final pos = event.raw.globalPosition / globalScale - toRect().topLeft;
    final isOpacity = await sprite!.isOpacityPixel(
      pos.toVector2(),
      scale: ownScale,
    );
    if (isOpacity) {
      resolver.push(OnTapAction(event), this);
    }
  }

  @mustCallSuper
  @override
  void render(Canvas canvas) {
    super.render(canvas);
    sprite?.render(
      canvas,
      size: size,
      overridePaint: overridePaint,
    );
  }

  /// Returns path to sprite with extension.
  Future<String> get pathSprite async {
    Fimber.i('Look at data `$pathData`...');
    if (!await isPathDataExists) {
      // this sprite without the data file
      // just return the sprite file name in the root picture
      return await isSpriteInRootFolder
          ? '$canvasName/$name.$allowedFileExtension'
          : '$canvasName/$name/$name.$allowedFileExtension';
    }

    // this sprite with data file, parse it
    final data = await Flame.assets.readJson(pathData);
    Fimber.i('data from `$pathData`:\n$data');
    final file = data['file'] as String?;

    return '$canvasName/$name/$file.$allowedFileExtension';
  }

  Future<bool> get isPathSpriteExists async => (await pathSprite).isAssetExists;

  Future<bool> get isSpriteInRootFolder async =>
      '$canvasName/$name.$allowedFileExtension'.isAssetExists;

  /// Look at data for welement depends of current state.
  String get pathData => buildPathData(canvasName, name, stateName);

  Future<bool> get isPathDataExists async => pathData.isAssetExists;

  static String buildPathData(
    String canvasName,
    String name,
    String stateName,
  ) =>
      'images/$canvasName/$name/$stateName/$dataFileName';

  @override
  Map<String, dynamic> toJson() => super.toJson()
    ..addAll({
      'nice scale': coverScale.n1,
      //'path sprite': pathSprite,
      //'is path sprite exists': isPathSpriteExists,
      'path data': pathData,
      //'is path data exists': isPathDataExists,
    });
}

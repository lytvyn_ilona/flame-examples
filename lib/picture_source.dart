import 'dart:io';

import 'package:fimber/fimber.dart';
import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

import 'extensions/asset_extension.dart';
import 'extensions/device_extension.dart';
import 'extensions/json_extension.dart';
import 'extensions/vector_extension.dart';
import 'picture_aspect_ratio.dart';
import 'welement_source.dart';

// Run in the terminal for generate this file:
//   flutter packages pub run build_runner build
// \see https://medium.com/flutter-community/generate-the-code-to-parse-your-json-in-flutter-c68aa89a81d9
//part 'picture_source.g.dart';

class PictureSource {
  static const fileName = 'picture';
  static const fileExtension = 'json';

  final String name;
  final Vector2 size;

  final PictureAspectRatio aspectRatio;

  final WelementSource background;
  final List<WelementSource> welementSourceList;

  get ws => welementSourceList;

  Orientation get orientation => PictureAspectRatio.orientation(size.toSize());

  bool get isLandscape => orientation.isLandscape;

  bool get isPortrait => orientation.isPortrait;

  PictureSource({
    required this.name,
    required this.size,
    required this.aspectRatio,
    required this.background,
    required List<WelementSource> ws,
  })   : assert(name.length > 0),
        assert(size.x > 0 && size.y > 0, 'The picture size should be natural.'),
        welementSourceList = ws;

  static Future<PictureSource?> fromAsset(
    String name,
    PictureAspectRatio aspectRatio,
  ) async {
    final path = await buildPathData(name, aspectRatio);
    Fimber.i('detected path for'
        ' screen ${aspectRatio.screenSize()}'
        ' ${aspectRatio.screenOrientation()}'
        ': `$path`');
    if (!await path.isAssetRootExists) {
      Fimber.e('Picture not found.'
          ' name `$name`'
          ' aspectRatio ${aspectRatio.screenSize()}'
          ' path `$path`');
      return null;
    }

    final data = await Flame.assets.readJson(path);
    Fimber.i('data from `$path`:\n$data');
    return PictureSource.fromJson(data, aspectRatio);
  }

  // \see Test 'demo picture recovered from not full JSON data'.
  factory PictureSource.fromFile(
    String path,
    PictureAspectRatio aspectRatio,
  ) =>
      PictureSource.fromString(File(path).readAsStringSync(), aspectRatio);

  factory PictureSource.fromString(
    String s,
    PictureAspectRatio aspectRatio,
  ) =>
      PictureSource.fromJson(s.json, aspectRatio);

  factory PictureSource.fromJson(
    Map<String, dynamic> json,
    PictureAspectRatio aspectRatio,
  ) {
    final name = (json['name'] as String).trim();
    final size = _prepareSize(json['size']);
    final background = WelementSource.fromJson(json['background']);

    final wsl = (json['ws'] ?? []) as List<dynamic>;
    final ws = wsl.map((s) => WelementSource.fromJson(s)).toList();

    return PictureSource(
      name: name,
      size: size,
      aspectRatio: aspectRatio,
      background: background,
      ws: ws,
    );
  }

  static Vector2 _prepareSize(dynamic d) {
    if (d is String) {
      return d.toVector2DependsOfSize(Vector2.zero());
    }

    final sizeAsList = d as List? ?? [];
    if (sizeAsList.length != 2) {
      Fimber.w('The picture size should be defined in the JSON.');
    }

    return (sizeAsList.length == 2)
        ? Vector2((sizeAsList[0] as num).toDouble(),
            (sizeAsList[1] as num).toDouble())
        : Vector2.zero();
  }

  Map<String, dynamic> toJson() {
    final r = <String, dynamic>{
      "name": name,
      "background": background.toJson(),
      "size": size.json,
      "aspectRatio": aspectRatio.json,
    };
    if (welementSourceList.isNotEmpty) {
      r['ws'] = welementSourceList.map((e) => e.toJson()).toList();
    }

    return r;
  }

  static Future<String> buildPathData(
    String pictureName,
    PictureAspectRatio aspectRatio,
  ) async {
    final aspectSize = aspectRatio.preferredScreenAspectSize();
    Fimber.i(
        'buildPathData for aspectSize $aspectSize ${aspectRatio.screenOrientation()}. '
        'If you can\'t find your new resolution add it to '
        '"PictureAspectRatio.aspectSizeList"');

    // 1) direct variant by screen size and device orientation
    {
      final aspectSuffix = aspectSize.likeAspectRatioSuffix;
      Fimber.i('1) aspectSuffix $aspectSuffix');
      if (await isPictureFileExists(pictureName, pictureFile(aspectSuffix))) {
        return pathPicture(pictureName, pictureFile(aspectSuffix));
      }
    }

    // 2) inverse orientation variant
    {
      final aspectSuffix = aspectSize.swapped.likeAspectRatioSuffix;
      Fimber.i('2) aspectSuffix $aspectSuffix');
      if (await isPictureFileExists(pictureName, pictureFile(aspectSuffix))) {
        return pathPicture(pictureName, pictureFile(aspectSuffix));
      }
    }

    // 3) first best available orientation variant
    for (final aspectSize in PictureAspectRatio.aspectSizeList) {
      final aspectSuffix = aspectSize.likeAspectRatioSuffix;
      Fimber.i('3) aspectSuffix $aspectSuffix');
      if (await isPictureFileExists(pictureName, pictureFile(aspectSuffix))) {
        return pathPicture(pictureName, pictureFile(aspectSuffix));
      }
    }

    return '';
  }

  /// Look at data for picture.
  static String pathPicture(String pictureName, String fileName) =>
      'images/$pictureName/$fileName';

  static Future<bool> isPictureFileExists(
    String pictureName,
    String fileName,
  ) async =>
      pathPicture(pictureName, fileName).isAssetRootExists;

  static String pictureFile(String aspectRatioSuffix) =>
      '${fileName}_$aspectRatioSuffix.$fileExtension';

  @override
  String toString() => toJson().sjson;

  @override
  bool operator ==(Object b) =>
      (b is PictureSource) &&
      name == b.name &&
      listEquals(welementSourceList, b.welementSourceList);

  @override
  int get hashCode => toJson().hashCode;
}
